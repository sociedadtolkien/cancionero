import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../auth/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public password: string = 'soy ainulindrim';
  public message: string = '';

  constructor(
    private router: Router,
    private loginService: LoginService,
  ) { }

  ngOnInit(): void {
  }

  public login() {
    if (this.loginService.login(this.password)) {
      this.message = '';
      this.router.navigateByUrl(this.loginService.getNextUrl());
    } else {
      this.message = 'Contraseña incorrecta';
    }
  }
}
