import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CancioneroApiService } from '../cancionero-api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public searchTerm: string = '';

  constructor(
    private router: Router,
    private cancioneroApi: CancioneroApiService,
  ) { }

  ngOnInit(): void {
  }

  public search() {
    this.router.navigate(['/canciones'],
                         {queryParams: {s: this.searchTerm}});
  }
}
