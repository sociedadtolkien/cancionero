import { Component, OnInit } from '@angular/core';
import { PWAInstallService } from '../pwa-install.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(
    private pwaInstallService: PWAInstallService,
  ) { }

  ngOnInit(): void {
  }

  public isInstallable() {
    return this.pwaInstallService.isAvailable();
  }

  public install() {
    this.pwaInstallService.install();
  }
}
