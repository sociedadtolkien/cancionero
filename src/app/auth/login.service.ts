import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private nextUrl: string = '/';

  constructor(
    private cookieService: CookieService
  ) { }

  public isLoggedIn() {
    const cookie = this.cookieService.get('ainulindale-access');
    return (cookie === 'true');
  }

  public setNextUrl(url: string) {
    this.nextUrl = url;
  }

  public getNextUrl(): string {
    return this.nextUrl;
  }

  public login(password: string) {
    // console.log('Contraseña cifrada', CryptoJS.MD5('la contraseña').toString());
    if (CryptoJS.MD5(password).toString() === 'a33ae29212887e1792e37652ffc50855') {
      this.cookieService.set('ainulindale-access', 'true', {expires: new Date(2038, 1, 1)});
      return true;
    } else {
      return false;
    }
  }
}
