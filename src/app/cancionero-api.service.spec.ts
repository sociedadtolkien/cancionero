import { TestBed } from '@angular/core/testing';

import { CancioneroApiService } from './cancionero-api.service';

describe('CancioneroApiService', () => {
  let service: CancioneroApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CancioneroApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
