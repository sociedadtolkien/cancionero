import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { CancioneroApiService, Cancion } from '../cancionero-api.service';

@Component({
  selector: 'app-canciones',
  templateUrl: './canciones.component.html',
  styleUrls: ['./canciones.component.scss']
})
export class CancionesComponent implements OnInit {

  public canciones$: Observable<Cancion[]>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private cancioneroApi: CancioneroApiService,
  ) {
    this.canciones$ = this.activatedRoute.queryParams.pipe(
      map((queryParams) => (queryParams.s || '')),
      switchMap((searchTerm: string) => this.cancioneroApi.search(searchTerm)),
    );
  }

  ngOnInit(): void {
  }

}
