import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, ReplaySubject, of } from 'rxjs';
import { map } from 'rxjs/operators';

interface CancionRaw {
  id: string;
  slug: string;
  link: string;
  tags: number[];
  title: {
    rendered: string;
  };
  acf: {
    cancion_tipo: string;
    cancion_autor: string;
    cancion_comentarios: string;
    cancion_titulo_original: string;
    cancion_autor_original: string;
    cancion_letra: string;
    cancion_materiales: {
      cancion_material_desc: string;
      cancion_material_doc: string;
      cancion_material_tipo: string;
    }[]
    cancion_enlaces: {
      cancion_enlace_titulo: string;
      cancion_enlace_url: string;
    }[]
  }
}

export interface Cancion {
  id: string;
  slug: string;
  link: string;
  title: string;
  tags: number[];
  tipo: string;
  autor: string;
  comentarios: string;
  titulo_original: string;
  autor_original: string;
  letra: string;
  materiales: {
    desc: string;
    doc: string;
    tipo: string;
  }[]
  enlaces: {
    titulo: string;
    url: string;
  }[]
}

function raw2Cancion(cancionRaw: CancionRaw): Cancion {
  return {
    id: cancionRaw.id,
    slug: cancionRaw.slug,
    link: cancionRaw.link,
    tags: cancionRaw.tags,
    title: cancionRaw.title.rendered,
    tipo: cancionRaw.acf.cancion_tipo,
    autor: cancionRaw.acf.cancion_autor,
    comentarios: cancionRaw.acf.cancion_comentarios,
    titulo_original: cancionRaw.acf.cancion_titulo_original,
    autor_original: cancionRaw.acf.cancion_autor_original,
    letra: cancionRaw.acf.cancion_letra,
    materiales: cancionRaw.acf.cancion_materiales ?
               cancionRaw.acf.cancion_materiales.map((materialRaw) => {
                 return {
                   desc: materialRaw.cancion_material_desc,
                   doc: materialRaw.cancion_material_doc,
                   tipo: materialRaw.cancion_material_tipo,
                 };
               }) : [],
    enlaces: cancionRaw.acf.cancion_enlaces ?
               cancionRaw.acf.cancion_enlaces.map((enlaceRaw) => {
                 return {
                   titulo: enlaceRaw.cancion_enlace_titulo,
                   url: enlaceRaw.cancion_enlace_url,
                 };
               }) : [],
  };
}

@Injectable({
  providedIn: 'root'
})
export class CancioneroApiService {

  private cancionesUrl = 'https://ainulindale.sociedadtolkien.org/wp-json/wp/v2/posts?categories=4&per_page=100&orderby=title&order=asc';
  private canciones$ = new ReplaySubject<Cancion[]>(1);

  constructor(private http: HttpClient) {
    this.getCancionesPage(1, []);
  }

  public getBySlug(slug: string): Observable<Cancion | undefined> {
    return this.canciones$.pipe(
      map((canciones) =>
        canciones.find((cancion) => cancion.slug === slug)
      )
    );
  }

  public search(searchTerm: string): Observable<Cancion[]> {
    return this.canciones$.pipe(
      map((canciones) =>
        canciones.filter((cancion) =>
         ((cancion.title.trim().toLowerCase().indexOf(searchTerm.trim().toLowerCase()) >= 0)
                   ||
            (cancion.autor.trim().toLowerCase().indexOf(searchTerm.trim().toLowerCase()) >= 0)
                   ||
           (!isNaN(parseInt(searchTerm)) && (cancion.tags.includes(parseInt(searchTerm)))
        ))))
    );
  }

  private getCancionesPage(pageNum: number, canciones: Cancion[]) {
    this.http.get<CancionRaw[]>(this.cancionesUrl + '&page=' + pageNum.toString(), {observe: 'response'})
      .subscribe((response) => {

        const cancionesPage = response.body!.map(raw2Cancion);
        const cancionesAcum = canciones.concat(cancionesPage);

        const totalPages = parseInt(response.headers.get('X-WP-TotalPages') || '1');
        if (totalPages <= pageNum) {
          this.canciones$.next(cancionesAcum);
        } else {
          this.getCancionesPage(pageNum + 1, cancionesAcum);
        }
      });
  }
}
