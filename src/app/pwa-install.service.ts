import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PWAInstallService {

  private installEvent: any | null = null;

  constructor() {
    window.addEventListener('beforeinstallprompt', (e) => {
      e.preventDefault();
      this.installEvent = e;
    });

    window.addEventListener('appinstalled', () => {
      this.installEvent = null;
    });
  }

  public isAvailable(): boolean {
    return (this.installEvent != null);
  }

  public async install() {
    this.installEvent.prompt();
    this.installEvent = null;
  }

};

