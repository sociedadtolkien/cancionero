import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { tap, map, switchMap } from 'rxjs/operators';

import { CancioneroApiService, Cancion } from '../cancionero-api.service';

@Component({
  selector: 'app-cancion',
  templateUrl: './cancion.component.html',
  styleUrls: ['./cancion.component.scss']
})
export class CancionComponent implements OnInit {

  public cancion$: Observable<Cancion | undefined>;
  public loading:boolean = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private cancioneroApi: CancioneroApiService,
  ) {
    this.cancion$ = this.activatedRoute.paramMap.pipe(
      map((paramMap) => paramMap.get('slug') || ''),
      tap((slug) => { this.loading = true }),
      switchMap((slug) => this.cancioneroApi.getBySlug(slug)),
      tap((slug) => { this.loading = false }),
    )
  }

  ngOnInit(): void { }

}
