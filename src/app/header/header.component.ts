import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() public title!: string;
  @Input() public subtitle!: string;
  @Input() public buttonLabel!: string;
  @Input() public buttonLink!: string[];

  constructor() { }

  ngOnInit(): void {
  }

}
