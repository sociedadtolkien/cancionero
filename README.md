# Cancionero STE

El cancionero oficial de la [Sociedad Tolkien Española](https://www.sociedadtolkien.org).

Aplicación PWA (progressve web app), realizada con Angular. Consiste en un front end, que
lee los datos del API REST del wordpress en https://ainulindale.sociedadtolkien.org, cachea
una copia local y permite hacer consultas.

## Probar en un entorno local

La aplicación ha sido generada con [Angular CLI](https://github.com/angular/angular-cli)
version 12.1.4. Para probarla en un entorno de desarrollo, se necesita [git](https://git-scm.com)
y [nodejs](https://nodejs.org) instalado.

Primero clonar el repo con:
```
git@gitlab.com:sociedadtolkien/cancionero.git
cd cancionero
```

Luego instalar todas las dependencias con:
```
npm install
```

Y arrancar la aplicación con:
```
npm run start
```

Para probarla, abrir con el navegador la dirección [http://localhost:4200].

Los dos primeros pasos sólo se necesita hacerlos una vez, el tercero siempre que se quiera usar.

